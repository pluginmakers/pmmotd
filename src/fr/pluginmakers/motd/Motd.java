package fr.pluginmakers.motd;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class Motd implements Listener {

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onServerListPing(final ServerListPingEvent e) {
		String motdserv = Main.motd;
		motdserv = motdserv.replace("&", "�");
		e.setMotd(motdserv);	
	}
	
}