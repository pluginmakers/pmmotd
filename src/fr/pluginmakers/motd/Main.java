package fr.pluginmakers.motd;

import java.util.logging.Logger;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	static Logger log = Logger.getLogger("minecraft");
    public static String motd = null;
	
    public void onEnable(){
    	if (!getConfig().contains("MOTD")) {
          getConfig().set("MOTD", "&4Welcome to the server !");
          saveConfig();
        }
    	
    	reloadConfig();
        motd = getConfig().getString("MOTD");
      	getServer().getPluginManager().registerEvents(new Motd(), this);
    }
 
}